var figlet = require('figlet');

figlet.text(process.argv.splice(2).join(' '), function (error, data) {
    if (error) {
        console.log(error);
    } else {
        console.log(data);
    }
});
